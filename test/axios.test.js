const { setupAxios, instance, noLogin } = require("../src/axios");

describe("Axios", () => {
  test("In global scope", async () => {
    setupAxios({
      baseURL: "https://api.thecatapi.com/v1/",
      "x-api-key": "446825e1-284d-4d4d-8fc7-7556636211ad"
    });

    const result = await instance().get("/breeds/search?q=bengal");
    expect(result[0].health_issues).toEqual(3);
  });

  test("NoLogin", async (done) => {
    setupAxios({
      baseURL: "https://api.thecatapi.com/v1/",
      "x-api-key": "446825e1-284d-4d4d-8fc7-7556636211ad"
    });

    noLogin().then(async (o) => {
      const result = await o.get("/breeds/search?q=bengal");
      expect(result[0].health_issues).toEqual(3);
      done();
    });
  });

  test("Singel login", async (done) => {
    setupAxios({
      baseURL: "https://api.thecatapi.com/v1/",
      "x-api-key": "446825e1-284d-4d4d-8fc7-7556636211ad"
    });
    Promise.all([
      instance()
        .get("/breeds/search?q=bengal")
        .then(async (result) => {
          expect(result[0].health_issues).toEqual(3);
          done();
        }),

      instance()
        .get("/breeds/search?q=toyger")
        .then(async (result) => {
          expect(result[0].health_issues).toEqual(2);
          done();
        })
    ]).then(() => {
      done();
    });
  });
});
