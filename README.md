# @lpgroup/feathers-utils

Collection of feathers-utils hooks and helper functions

## Install

Installation of the npm

```sh
echo @lpgroup:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npm install @lpgroup/feathers-utils
```

## Example

```javascript
const utils = require("@lpgroup/feathers-utils");

```

## Contribute

See https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md

## License

MIT, see LICENSE.MD

## API

### `xxx`

#### `xxx(objSchema)`

```js
xxx(xxx);
```
