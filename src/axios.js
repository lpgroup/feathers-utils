/**
 * Feathersjs wrapper of axios http client.
 */
const debug = require("debug")("axios");
const debugError = require("debug")("axios:error");
const axios = require("axios");
const rateLimit = require("axios-rate-limit");
const { NotFound, Timeout } = require("@feathersjs/errors");
const { checkExpected } = require("./compare");

/**
 * setupAxios({baseURL: "http://cybercow.se", headers: { "X-Custom-Header": "LPGroup" }})
 */
let _createOptions = {
  timeout: 60000,
  headers: { "X-Custom-Header": "lpGroup" }
};

let _baseURL;
let _errorHandlerWithException = true;

//TODO: let _requiredKeys = [];
let _ignoreKeyCompare = [];

let _globalUser = undefined;
let _globalPassword = undefined;

function setupAxios(options = {}) {
  if (options.baseURL) _baseURL = options.baseURL.replace(/\/$/, "");
  if (options.timeout) _createOptions.timeout = options.timeout;
  if (options.customHeader) _createOptions.headers["X-Custom-Header"] = options.customHeader;
  if (options["x-api-key"]) _createOptions.headers["x-api-key"] = options["x-api-key"];
  if (options.headers) _createOptions.headers = { ..._createOptions.headers, ...options.headers };

  if ("errorHandlerWithException" in options)
    _errorHandlerWithException = options.errorHandlerWithException;

  //TODO: if ("requiredKeys" in options) _requiredKeys = options.requiredKeys;
  if ("ignoreKeyCompare" in options) _ignoreKeyCompare = options.ignoreKeyCompare;

  if ("user" in options) _globalUser = options.user;
  if ("password" in options) _globalPassword = options.password;
}

// TODO: Change name?
// Other files are accessing this through, and that might be a bit confusing.
// const { instance } = require("@lpgroup/feathers-utils");
/**
 *
 * @param {*} options
 * @param {*} reuseRawInstance An axios rateLimit object to reuse on this instance.
 */
function instance(options = {}, reuseRawInstance = undefined) {
  return {
    user: undefined,
    password: undefined,
    accessToken: undefined,
    rawInstance:
      reuseRawInstance ||
      rateLimit(axios.create({ ..._createOptions, ...options }), {
        maxRequests: 3,
        perMilliseconds: 500
      }),

    /** Returns axios instance with login headers */
    ins() {
      if (this.accessToken)
        this.rawInstance.defaults.headers.common["Authorization"] = this.accessToken;
      return this.rawInstance;
    },

    // TOOD: Wait longer and longer
    async waitOnServerReady(url) {
      for (let x = 0; x < 100; x++) {
        try {
          const result = await this.ins().get(url);
          if (result != undefined && result.status == 200) {
            debug(`Connected to ${url}`);
            return;
          }
          // eslint-disable-next-line no-empty
        } catch (error) {}
        await _sleep(500);
        debugError(`  Wait on server ${url}`);
      }
      throw new Error("Server is not responding");
    },

    async closePlugin() {},

    async login(user, password) {
      const newInstance = instance(options, this.rawInstance);
      newInstance.user = user || _globalUser;
      newInstance.password = password || _globalPassword;

      const fullUrl = _url("/authentication");
      return this.rawInstance
        .post(fullUrl, {
          strategy: "local",
          email: newInstance.user,
          password: newInstance.password
        })
        .then((response) => {
          debug(`Logged in as ${response.data.user.email}`);
          newInstance.accessToken = response.data.accessToken;
          return newInstance;
        })
        .catch(newInstance._errorHandler("login", fullUrl));
    },

    async post(url, data) {
      const fullUrl = _url(url);
      return this.ins()
        .post(fullUrl, data)
        .then(this._responseHandler("post", fullUrl))
        .catch(this._errorHandler("post", fullUrl));
    },

    async get(url) {
      const fullUrl = _url(url);
      return this.ins()
        .get(fullUrl)
        .then(this._responseHandler("get", fullUrl, options))
        .catch(this._errorHandler("get", fullUrl));
    },

    async patch(url, data) {
      const fullUrl = _url(url);
      return this.ins()
        .patch(fullUrl, data)
        .then(this._responseHandler("patch", fullUrl))
        .catch(this._errorHandler("patch", fullUrl));
    },

    async put(url, data) {
      const fullUrl = _url(url);
      return this.ins()
        .put(fullUrl, data)
        .then(this._responseHandler("put", fullUrl))
        .catch(this._errorHandler("put", fullUrl));
    },

    async remove(url) {
      const fullUrl = _url(url);
      return this.ins()
        .delete(fullUrl)
        .then(this._responseHandler("delete", fullUrl))
        .catch(this._errorHandler("delete", fullUrl));
    },

    // TODO: Should handle other than pdf
    async downloadFile(url) {
      const fullUrl = _url(url);
      const file = await this.ins()
        .get(url, {
          responseType: "arraybuffer",
          headers: {
            Accept: "application/pdf"
          }
        })
        .then(this._responseHandler("download", fullUrl))
        .catch(this._errorHandler("download", fullUrl));

      if (file) return file.toString("base64");
      else throw Error("Can't download file " + url);
    },

    async postPut(url, key, data) {
      return this._multiCallWithKey(this._postPut, url, key, data);
    },

    async _postPut(url, key, data) {
      const fullUrl = _url(url);
      return this.ins()
        .post(fullUrl, data)
        .then(this._responseHandler("postPut", fullUrl))
        .catch(async (error) => {
          const message = error.response.data.message;
          if (message.startsWith("E11000 duplicate key error collection")) {
            const _id = data[key];
            if (_id) {
              return this.put(`${url}/${_id}`, data);
            } else {
              debugError(`  POST: ${fullUrl} `);
              debugError(`    Missing ${key} in ${data}`);
            }
          } else {
            this._errorHandler("postPut", fullUrl)(error);
          }
        });
    },

    _multiCallWithKey(func, id, key, data) {
      if (Array.isArray(data)) {
        return Promise.all(
          data.map(async (x) => {
            return func.call(this, id, key, x);
          })
        );
      } else {
        return func.call(this, id, key, data);
      }
    },

    _responseHandler(method, fullUrl, options = {}) {
      if (options.waitUntilExpected) return this._responseHandlerWithWait(method, fullUrl, options);
      else return this._responseHandlerNoWait(method, fullUrl, options);
    },

    _responseHandlerNoWait(method, fullUrl, options) {
      return (response) => {
        debug(`  ${method}: ${this.user || ""} ${fullUrl} `);
        checkExpected(response.data, { _ignoreKeyCompare, ...options });
        return response.data;
      };
    },

    _responseHandlerWithWait(method, fullUrl, options) {
      return async (response) => {
        if (options.waitUntilExpected > 1) process.stdout.write(".");
        else debug(`  ${method}: ${this.user || ""} ${fullUrl} `);

        if (
          checkExpected(response.data, { _ignoreKeyCompare, ...options }) &&
          options.waitUntilExpected
        ) {
          await _sleep(1000);
          options.waitUntilExpected++;
          return this.get(fullUrl, options);
        }
        return response.data;
      };
    },

    _errorHandler(method, fullUrl) {
      return (err) => {
        if (_errorHandlerWithException) this._errorHandlerExceptions(method, fullUrl, err);
        else this._errorHandlerConsole(method, fullUrl, err);
      };
    },

    _errorHandlerConsole(method, fullUrl, err) {
      debugError(`  ${method}: ${fullUrl} ${err.message} ${err.code}`);
    },

    _errorHandlerExceptions(method, fullUrl, err) {
      debugError(`  ${method}: ${fullUrl} ${err.message} ${err.code}`);
      if (err.response && err.response.data && err.response.data.code == 404) {
        throw new NotFound(err.response.data.message);
      } else if (err.code == "ECONNABORTED" || err.code == "ECONNRESET") {
        throw new Timeout("Internal timeout");
      } else {
        throw err;
      }
    }
  };
}

async function login(user, password) {
  const loginInstance = instance();
  return loginInstance.login(user, password);
}

async function noLogin(options = {}) {
  return instance(options);
}

function _url(url) {
  return `${_baseURL || ""}${url}`.toLowerCase();
}

function _sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

module.exports = { setupAxios, login, instance, noLogin };
