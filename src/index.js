// Hooks
const debugHook = require("./hooks/debug-hook");
const disableSync = require("./hooks/disable-sync");
const disallowEnvironment = require("./hooks/disallow-environment");
const requestLog = require("./hooks/request-log");
const setQuery = require("./hooks/set-query");
const url = require("./hooks/url");

// Middleware
const protocol = require("./middleware/protocol");

// Plugins
const pluginUtils = require("./plugins/utils");
const axios = require("./plugins/axios");
const gcs = require("./plugins/gcs");
const mongodb = require("./plugins/mongodb");
const nats = require("./plugins/nats");
const rabbitmq = require("./plugins/rabbitmq");
const sync = require("./plugins/sync");

// Common utils
const axiosCommon = require("./axios");
const common = require("./common");

module.exports = {
  hooks: {
    debugHook,
    disableSync,
    disallowEnvironment,
    requestLog,
    setQuery,
    url
  },
  protocol,
  ...pluginUtils,
  axios,
  gcs,
  mongodb,
  nats,
  rabbitmq,
  sync,
  ...axiosCommon,
  ...common
};
