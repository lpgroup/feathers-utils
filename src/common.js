const { getItems } = require("feathers-hooks-common");

function buildItemHook(cb) {
  return async (context) => {
    const _items = getItems(context);
    const items = Array.isArray(_items) ? _items : [_items];

    for (const item of items) {
      cb(context, item);
    }

    return context;
  };
}

/**
 * Create a fethersjs params object to use on internal calls.
 */
function internalParams(params, query = {}) {
  return { ...params, query, provider: "" };
}

module.exports = { buildItemHook, internalParams };
