const dbgRequest = require("debug")("req.ext");
const debInternal = require("debug")("req.srv");
const dbgException = require("debug")("exception");
const { TransactionAborted } = require("@lpgroup/feathers-mongodb-hooks").exceptions;

// All external providers that can exist in params.provider.
const providers = ["rest", "socketio", "primus"];

module.exports = function () {
  return (context) => {
    if (context.type == "before") {
      const url = getUrl(context);
      if (url) {
        if (providers.includes(context.params.provider)) dbgRequest(`${context.method} ${url}`);
        else debInternal(`${context.method} ${url}`);
      }
    }

    if (
      context.error &&
      !context.result &&
      context.error.type != "FeathersError" &&
      !(context.error instanceof TransactionAborted)
    ) {
      if (
        context.error.message == "Current topology does not support sessions" ||
        context.error.codeName == "NoSuchTransaction"
      ) {
        dbgException(context.error.message);
      } else {
        dbgException(context.error.stack);
      }
    }
  };
};

const ignoreUrls = ["ready", "healthy"];
function getUrl(context) {
  let url = context.path;
  for (const key in context.params.query) {
    url = url.replace(":" + key, context.params.query[key]);
  }
  if (context.id) url += `/${context.id}`;

  if (ignoreUrls.includes(url)) return "";
  else return url.charAt(0) == "/" ? url : "/" + url;
}
