/**
 * If a system environment variable is set to a specific value, throw 404 error.
 *
 * Example
 * module.exports = {
 *  before: {
 *    all: [disallowEnvironment({ NODE_CONFIG_ENV: "prod" })],
 *  }
 * };
 *
 */
const { NotFound } = require("@feathersjs/errors");

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async (context) => {
    for (const option in options) {
      if (process.env[option].toLowerCase() == options[option].toLowerCase()) throw new NotFound();
    }
    return context;
  };
};
