/**
 * Add url key to json row.
 */
// eslint-disable-next-line no-unused-vars
module.exports = function (options = { key: "_id" }) {
  return async (context) => {
    if (context.params._calledByHook) return;

    const { app, result, params } = context;
    const host = app.get("mainDomain");

    let url = context.path;
    for (const key in params.query) {
      url = url.replace(":" + key, params.query[key]);
    }

    // Is it used by find or get
    const localResult = result.data ? result.data : [result];
    for (const data of localResult) {
      data.url = host.concat("/", url);
      if (options.key) data.url += "/" + data[options.key];
    }

    return context;
  };
};
