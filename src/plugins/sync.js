const debugSyncFull = require("debug")("syncFull");
const debugSync = require("debug")("sync");
const { core } = require("feathers-sync");
const { onPluginReady } = require("./utils");

async function init(app) {
  app.sync = await configureSync(app);
  return app.sync;
}

async function initWithOptions() {
  console.error("Need app");
  process.exit(1);
}

async function configureSync(app) {
  app.configure(core);

  // NOTE: Need to configure nats before configure sync
  const nc = await onPluginReady("nats");
  const sync = {
    type: "nats",
    ready: new Promise((resolve, reject) => {
      nc.on("connect", (c) => {
        resolve(c);
        console.log("  Sync ready");
      });

      nc.on("error", (err) => {
        reject(err);
      });
    }),
    serialize: (value) => {
      const sessionId = value.context.params.sessionId;
      const session = value.context.params.mongodb.session;
      delete value.context.params.sessionId;
      delete value.context.params.mongodb.session;
      const result = JSON.stringify(value);
      if (sessionId) {
        value.context.params.sessionId = sessionId;
        value.context.params.mongodb.session = session;
      }
      return result;
    },
    deserialize: JSON.parse
  };

  // Sent every time a service
  app.on("sync-out", (data) => {
    debugSync("out");
    debugSyncFull("out", data);
    nc.publish("sync", data);
  });

  nc.subscribe("sync", (data) => {
    debugSync("in");
    debugSyncFull("in", data);
    app.emit("sync-in", data);
  });

  return sync;
}

module.exports = { init, initWithOptions };
