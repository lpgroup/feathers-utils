const MongoClient = require("mongodb").MongoClient;
const { startSession, endSession, errorSession } = require("@lpgroup/feathers-mongodb-hooks").hooks;

// System globals to access MongoDb
let client = null;
let database = null;

async function init(app) {
  const uri = app.get("mongodb");
  return initWithOptions({ uri });
}
async function initWithOptions(options) {
  if (!client) {
    const db = await createMongoClient(options.uri);
    return { client, database: db, closePlugin };
  }
}

async function createMongoClient(uri) {
  const databaseName = uri.substr(uri.lastIndexOf("/") + 1);
  client = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  database = client
    .connect()
    .then((client) => {
      console.log(`Connected to Mongo db: ${databaseName}`);
      const db = client.db(databaseName);
      //TODO Remove
      db.createIndexThrowError = createIndexThrowError;
      return db;
    })
    .catch((error) => {
      console.log("Mongo error: ", error);
    });
  return await database;
}

async function closePlugin() {
  this.db.close();
  this.client.close();
}

/**
 * Regular mongodb createIndex with error catcher.
 */
async function createIndexThrowError(table, fieldOrSpec, options) {
  return this.collection(table)
    .createIndex(fieldOrSpec, options)
    .then((index) => {
      console.log(`Created index: ${table}.${index}`);
    })
    .catch((err) => {
      console.error("error", err.message);
      process.exit(1);
    });
}

function startSessionOveride() {
  return async (context) => {
    const startTrx = startSession({ client, database: await database });
    return startTrx(context);
  };
}

module.exports = {
  init,
  initWithOptions,
  startSession: startSessionOveride,
  endSession,
  errorSession
};
