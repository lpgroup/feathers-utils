const { setupAxios, instance } = require("../axios");

async function init(app, options) {
  return initWithOptions(options);
}

async function initWithOptions(options) {
  setupAxios(options);
  return instance();
}

module.exports = { init, initWithOptions };
