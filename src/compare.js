const deepDiff = require("deep-diff").diff;

/**
 *
 * @param {*} response
 * @param {*} options {
 *   expected: {},
 *   ignoreKeCompare: ["key"]
 * }
 */
function checkExpected(response, options) {
  if ("expected" in options) {
    const { ignoreKeyCompare = [] } = options;
    // TODO: Fungerar utan denna?
    // if (!requiredKeys.every((k) => k in response)) {
    //   debug(`Response need the following keys ${requiredKeys}`);
    // }

    const diff = _removeEditedKeys(deepDiff(response, options.expected), ignoreKeyCompare);
    if (diff.length > 0) {
      if (!options.waitUntilExpected) {
        console.error("Response: ");
        console.error(JSON.stringify(response, null, "  "));
        console.error("Diff: ");
        console.error(JSON.stringify(diff, null, "  "));
      }
      return true;
    }
  }
  return false;
}

function _removeEditedKeys(diff, ignoreKeyCompare) {
  if (diff == undefined) return [];
  return diff.filter((row) => {
    if ("path" in row) {
      return (
        row.kind != "E" || (row.kind == "E" && !row.path.some((v) => ignoreKeyCompare.includes(v)))
      );
    } else {
      return true;
    }
  });
}

module.exports = { checkExpected };
