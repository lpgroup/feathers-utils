module.exports = () => {
  // Add what protocol that is used to params in all services.
  return function protocol(req, res, next) {
    req.feathers.protocol = req.protocol;
    next();
  };
};
